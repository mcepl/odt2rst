import logging
import os
import os.path
import unittest
import odt2rst


class TestDocs(unittest.TestCase):
    def setUp(self):
        self.maxDiff = None

    def assertFileEqual(self, expected_file, observed_file, msg=None):
        exp_str = open(expected_file, 'r').readlines()
        obs_str = open(observed_file, 'r').readlines()
        self.assertEqual(exp_str, obs_str, msg)

    def _testFile(self, basename):
        basedir = os.path.dirname(os.path.realpath(__file__))
        odt_fn = os.path.join(basedir, '%s.odt' % basename)
        exp_rst_fn = os.path.join(basedir, '%s.expected.rst' % basename)
        obs_rst_fn = os.path.join(basedir, '%s.rst' % basename)
        logging.debug('odt_fn = %s', odt_fn)
        logging.debug('obs_rst_fn = %s', obs_rst_fn)
        logging.debug('exp_rst_fn = %s', exp_rst_fn)
        odt2rst.odt2rst(odt_fn, obs_rst_fn, odt2rst.Options())
        self.assertFileEqual(exp_rst_fn, obs_rst_fn)
        os.remove(obs_rst_fn)

    def test_rationalism(self):
        self._testFile('rationalism-in-politics')

    def test_preformatted(self):
        self._testFile('preformatted_text')

if __name__ == '__main__':
    unittest.main()

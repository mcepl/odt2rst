
.. saved from:http://www.mugu.com/cgi-bin/Upstream/oakeshott-rationalism-politics?embedded=yes&cumulative_category_title=Michael+Oakeshott&cumulative_category_id=Oakeshott



Rationalism in politics
=======================

Michael Oakeshott `*Cambridge Journal*`_ , Volume I, 1947.

.. _`*Cambridge Journal*`: https://en.wikipedia.org/wiki/Cambridge_Journal_of_Economics

Les grands hommes.
en apprenant aux*Maxims et Reflexions* , 221 


ONE
---

The object of this essay is to consider the character and pedigree of the most remarkable intellectual fashion of post-Renaissance Europe.
The Rationalism with which I am concerned is modern Rationalism.
No doubt its surface reflects the light of rationalisms of a more distant past, but in its depth there is a quality exclusively its own, and it is this quality that I propose to consider, and to consider mainly in its impact upon European politics.
What I call Rationalism in politics is not, of course, the only (and it is certainly not the most fruitful) fashion in modern European political thinking.
But it is a strong and a lively manner of thinking which, finding support in its filiation with so much else that is strong in the intellectual composition of contemporary Europe, has come to colour the ideas, not merely of one, but of all political persuasions, and to flow over every party line.
By one road or another, by conviction, by its supposed inevitability, by its alleged success, or even quite unreflectively, almost all politics today have become Rationalist or near-Rationalist.

The general character and disposition of the Rationalist are, I think., difficult to identify.
At bottom he stands (he always stands) for independence of mind on all occasions, for thought free from obligation to any authority save the authority of reason'.
His circumstances in the modern world have made him contentious: he is the enemy of authority, of prejudice, of the merely traditional, customary or habitual.
His mental attitude is at once sceptical and optimistic: sceptical, because there is no opinion, no habit, no belief, nothing so firmly rooted or so widely held that he hesitates to question it and to judge it by what he calls his 'reason'; optimistic, because the Rationalist never doubts the power of his 'reason (when properly applied) to determine the worth of a thing, the truth of an opinion or the propriety of an action.
Moreover, he is fortified by a belief in a reason' common to all mankind, a common power of rational consideration, which is the ground and inspiration of argument: set up on his door is the precept of Parmenides—judge by rational argument.
But besides this, which gives the Rationalist a touch of intellectual equalitarianism, he is something also of an individualist, finding it difficult to believe that anyone who can think honestly and clearly will think differently from himself.

But it is an error to attribute to him an excessive concern with a priori argument.
He does not neglect experience, but he often appears to do so because he insists always upon it being his own experience (wanting to begin everything de novo), and because of the rapidity with which he reduces the tangle and variety of experience to a set of principles which he will then attack or defend only upon rational grounds.
He has no sense of the cumulation of experience, only of the readiness of experience when it has been converted into a formula: the past is significant to him only as an encumbrance He has none of that negative capability (which Keats attributed to Shakespeare), the power of accepting the mysteries and uncertainties of experience without any irritable search for order and distinctness, only the capability of subjugating experience; he has no aptitude for that close and detailed appreciation of what actually presents itself which Lichtenberg called negative enthusiasm, but only the power of recognizing the large outline which a general theory imposes upon events.
His cast of mind is gnostic, and the sagacity of Ruhnken's rule, *Oportet quaedam nescire* , is lost upon him.
There are some minds which give us the sense that they have passed through an elaborate education which was designed to initiate them into the traditions and achievements of their civilization; the immediate impression we have of them is an impression of cultivation, of the enjoyment of an inheritance.
But this is not so with the mind of the Rationalist, which impresses us as, at best, a finely tempered, neutral instrument, as a well-trained rather than as an educated mind.
Intellectually, his ambition is not so much to share the experience of the race as to be demonstrably a self-made man.
And this gives to his intellectual and practical activities an almost preternatural deliberateness and self-consciousness, depriving them of any element of passivity, removing from them all sense of rhythm and continuity and dissolving them into a succession of climacterics, each to be surmounted by a tour de raison.
His mind has no atmosphere, no changes of season and temperature; his intellectual processes, so far as possible, are insulated from all external influence and go on in the void.
And having cut himself off from the traditional knowledge of his society, and denied the value of any education more extensive than a training in a technique of analysis, he is apt to attribute to mankind a necessary inexperience in all the critical moments of life, and if he were more self-critical he might begin to wonder how the race had ever succeeded in surviving.
With an almost poetic fancy, he strives to live each day as if it were his first, and he believes that to form a habit is to fail.
And if, with as yet no thought of analysis, we glance below the surface, we may, perhaps, see in the temperament, if not in the character, of the Rationalist, a deep distrust of time, an impatient hunger for eternity and an irritable nervousness in the face of everything topical and transitory.

Now, of all worlds, the world of politics might seem the least amenable to rationalist treatment—politics, always so deeply veined with both the traditional, the circumstantial and the transitory.
And, indeed, some convinced Rationalists have admitted defeat here: Clemenceau, intellectually a child of the modern Rationalist tradition (in his treatment of morals and religion, for example), was anything but a Rationalist in politics.
But not all have admitted defeat.
If we except religion, the greatest apparent victories of Rationalism have been in politics: it is not to be expected that whoever is prepared to carry his rationalism into the conduct of life will hesitate to carry it into the conduct of public affairs.
[1]_

.. [1] A faithful account of the politics of rationalism (with all its confusions and ambivalences) is to be found in H.J. Blackham. *Political Discipline in a Free Society* .

But what is important to observe in such a man (for it is characteristic) is not the decisions and actions he is inspired to make, but the source of his inspiration, his idea (and with him it will be a deliberate and conscious idea) of political activity.
He believes, of course, in the open mind, the mind free from prejudice and its relic, habit.
He believes that the unhindered human 'reason' (if only it can be brought to bear) is an infallible guide in political activity.
Further, he believes in argument as the technique and operation of reason'; the truth of an opinion and the 'rational' ground (not the use) of an institution is all that matters to him.
Consequently, much of his political activity consists in bringing the social, political, legal and institutional inheritance of his society before the tribunal of his intellect; and the rest is rational administration, 'reason' exercising an uncontrolled jurisdiction over the circumstances of the case.
To the Rationalist, nothing is of value merely because it exists (and certainly not because it has existed for many generations), familiarity has no worth, and nothing is to be left standing for want of scrutiny.
And his disposition makes both destruction and creation easier for him to understand and engage in, than acceptance or reform.
To patch up, to repair (that is, to do anything which requires a patient knowledge of the material), he regards as waste of time: and he always prefers the invention of a new device to making use of a current and well-tried expedient.
He does not recognize change unless it is a self-consciously induced change, and consequently he falls easily into the error of identifying the customary and the traditional with the changeless.
This is aptly illustrated by the rationalist attitude towards a tradition of ideas.
There is, of course, no question either of retaining or improving such a tradition, for both these involve an attitude of submission.
It must be destroyed.
And to fill its place the Rationalist puts something of his own making—an ideology, the formalized abridgment of the supposed substratum of rational truth contained in the tradition

The conduct of affairs, for the Rationalist, is a matter of solving problems, and in this no man can hope to be successful whose reason has become inflexible by surrender to habit or is clouded by the fumes of tradition.
In this activity the character which the Rationalist claims for himself is the character of the engineer, whose mind (it is supposed) is controlled throughout by the appropriate technique and whose first step is to dismiss from his attention everything not directly related to his specific intentions.
This assimilation of politics to engineering is, indeed, what may be called the myth of rationalist politics.
And it is, of course, a recurring theme in the literature of Rationalism.
The politics it inspires may be called the politics of the felt need; for the Rationalist, politics are always charged with the feeling of the moment.
He waits upon circumstance to provide him with his problems, but rejects its aid in their solution.
That anything should be allowed to stand between a society and the satisfaction of the felt needs of each moment in its history must appear to the Rationalist a piece of mysticism and nonsense.
And his politics are, in fact, the rational solution of those practical conundrums which the recognition of the sovereignty of the felt need perpetually creates in the life of a society.
Thus, political life is resolved into a succession of crises, each to be surmounted by the application of reason'.
Each generation, indeed, each administration, should see unrolled before it the blank sheet of infinite possibility.
And if by chance this tabula rasa has been defaced by the irrational scribblings of tradition-ridden ancestors, then the first task of the Rationalist must be to scrub it clean; as Voltaire remarked, the only way to have good laws is to burn all existing laws and to start afresh.
[2]_

.. [2]  Cf. Plato. *Republic*. 501A. The idea that you can get rid of a law by burning it is characteristic of the Rationalist. who can think of a law only as something written down.

Two other general characteristics of rationalist politics may be observed.
They are the politics of perfection, and they are the politics of uniformity; either of these characteristics without the other denotes a different style of politics.
The essence of rationalism is their combination.
The evanescence of imperfection may be said to be the first item of the creed of the Rationalist.
He is not devoid of humility; he can imagine a problem which would remain impervious to the onslaught of his own reason.
But what he cannot imagine is politics which do not consist in solving problems, or a political problem of which there is no 'rational' solution at all.
Such a problem must be counterfeit.
And the 'rational' solution of any problem is, in its nature, the perfect solution.
There is no place in his scheme for a 'best in the circumstances', only a place for 'the best'; because the function of reason is precisely to surmount circumstances.
Of course, the Rationalist is not always a perfectionist in general, his mind governed in each occasion by a comprehensive Utopia; but invariably he is a perfectionist in detail.
And from this politics of perfection springs the politics of uniformity; a scheme which does not recognize circumstance can have no place for variety.
'There must in the nature of things be one best form of government which all intellects, sufficiently roused from the slumber of savage ignorance, will be irresistibly incited to approve,' writes Godwin.
This intrepid Rationalist states in general what a more modest believer might prefer to assert only in detail; but the principle holds—there may not be one universal remedy for all political ills, but the remedy for any particular ill is as universal in its application as it is rational in its conception.
If the rational solution for one of the problems of a society has been determined, to permit any relevant part of the society to escape from the solution is, ex hypothesis, to countenance irrationality.
There can be no place for preferences that is not rational preference, and all rational preferences necessarily coincide.
Political activity is recognized as the imposition of a uniform condition of perfection upon human conduct.

The modern history of Europe is littered with the projects of the politics of Rationalism.
The most sublime of these is, perhaps, that of Robert Owen for 'a world convention to emancipate the human race from ignorance, poverty, division, sin and misery'—so sublime that even a Rationalist (but without much justification) might think it eccentric.
But not less characteristic are the diligent search of the present generation for an innocuous power which may safely be made so great as to be able to control all other powers in the human world, and the common disposition to believe that political machinery can take the place of moral and political education.
The notion of founding a society, whether of individuals or of States, upon a Declaration of the Rights of Man is a creature of the rationalist brain, so also are 'national' or racial self-determination when elevated into universal principles.
The project of the so-called Re-union of the Christian Churches, of open diplomacy, of a single tax, of a civil service whose members 'have no qualifications other than their personal abilities', of a self-consciously planned society, the Beveridge Report, the Education Act of 1944, Federalism, Nationalism, Votes for Women, the Catering Wages Act, the destruction of the Austro-Hungarian Empire, the World State (of H. G. Wells or anyone else), and the revival of Gaelic as the official language of fire, are alike the progeny of Rationalism.
The odd generation of rationalism in politics is by sovereign power out of romanticism.


TWO
---

The placid lake of Rationalism lies before us in the character and disposition of the Rationalist, its surface familiar and not unconvincing.
its waters fed by many visible tributaries.
But in its depths there flows a hidden spring, which, though it was riot the original fountain from which the lake grew, is perhaps the pre-eminent source of its endurance.
This spring is a doctrine about human knowledge.
That some such fountain lies at the heart of Rationalism will not surprise even those who know only its surface: the superiority of the unencumbered intellect lay precisely in the fact that it could reach more, and more certain, knowledge about man and Society than was otherwise possible: the superiority of the ideology Over the tradition lay in its greater precision and its alleged demonstrability.
Nevertheless, it is not, properly speaking, a philosophical theory of knowledge, and it can be explained with agreeable informality.

Every science, every art, every practical activity requiring skill of any sort, indeed every human activity whatsoever, involves knowledge.
And, universally, this knowledge is of two sorts, both of which are always involved in any actual activity.
It is not, I think, making too much of it to call them two sorts of knowledge, because (though in fact they do not exist separately) there are certain important differences between them.
The first sort of knowledge I will call technical knowledge or knowledge of technique.
In every art and science, and in every practical activity, a technique is involved.
In many activities this technical knowledge is formulated into rules which are, or may be, deliberately learned, remembered, and, as we say, put into practice; but whether or not it is, or has been, precisely formulated, its chief characteristic is that it is susceptible of precise formulation, although special skill and insight may be required to give it that formulation.
[3]_ The technique (or part of it) of driving a motor car on English roads is to be found in the Highway Code, the technique of cookery is contained in the cookery book, and the technique of discovery in natural science or in history is in their rules of research, of observation and verification.
The second sort of knowledge I will call practical, because it exists only in use, is not reflective and (unlike technique) cannot be formulated in rules.
This does not mean, however, that it is an esoteric sort of knowledge.
It means only that the method by which it may be shared and becomes common knowledge is not the method of formulated doctrine.
And if we consider it from this point of view, it would not, I think, be misleading to speak of it as traditional knowledge.
In every activity this sort of knowledge is also involved; the mastery of any skill, the pursuit of any concrete activity is impossible without it.

.. [3] G. Polya. *How to Solve It*.

These two sorts of knowledge, then, distinguishable but inseparable, are the twin components of the knowledge involved in every concrete human activity.
In a practical art, such as cookery, nobody supposes that the knowledge that belongs to the good cook is confined to what is or may be written down in the cookery book; technique and what I have called practical knowledge combine to make skill in cookery wherever it exists.
And the same is true of the fine arts, of painting, of music, of poetry; a high degree of technical knowledge, even where it is both subtle and ready, is one thing; the ability to create a work of art, the ability to compose something with real music qualities, the ability to write a great sonnet, is another, and requires, in addition to technique, this other sort of knowledge.
Again, these two sorts of knowledge are involved in any genuinely scientific activity.
[4]_ The natural scientist will certainly make use of the rules of observation and verification that belong to his technique, but these rules remain only one of the components of his knowledge; advance in scientific discovery was never achieved merely by following the rules.
[5]_ The same situation may be observed also in religion.
It would, I think, be excessively liberal to call a man a Christian who was wholly ignorant of the technical side of Christianity, who knew nothing of creed or formulary, but it would be even more absurd to maintain that even the readiest knowledge of creed and catechism ever constituted the whole of the knowledge that belongs to a Christian.
And what is true of cookery, of painting, of natural science and of religion, is no less true of politics: the knowledge involved in political activity is both technical and practica1. [6]_ Indeed, as in all arts which have men as their plastic material, arts such as medicine, industrial management, diplomacy, and the art of military command, the knowledge involved in political activity is pre-eminently of this dual character.
Nor, in these arts, is it correct to say that whereas technique will tell a man (for example, a doctor) what to do, it is practice which tells him how to do it—the 'bed-side manner', the appreciation of the individual with whom he has to deal.

.. [4] Some excellent observations on this topic are to be found in M. Polanyi. *Science. Faith and Society*.



.. [5] Polya. for example. in spite of the fact that his book is concerned with heuristic. suggests that the root renditions of success in scientific research are, first, 'to have brains and good luck`, and secondly, 'to sit tight and wait till you get a bright idea', neither of which are technical rules.



.. [6] Thucydides puts an appreciation of this truth into the mouth of Pericles. To be a politician and to refuse the guidance of technical knowledge is. for Pericles. a piece of folly. And yet the main theme of the Funeral Oration is not the value of technique in politics, but the value of practical and traditional knowledge. ii, 40.

Even in the what, and above all in diagnosis, there lies already this dualism of technique and practice: there is no knowledge which is not 'know how'.
Nor, again, does the distinction between technical and practical knowledge coincide with the distinction between a knowledge of means and a knowledge of ends, though on occasion it may appear to do so.
In short, nowhere, and pre-eminently not in political activity, can technical knowledge be separated from practical knowledge, and nowhere can they be considered identical with one another or able to take the place of one another.
[7]_

.. [7] Duke Huan of Ch`i was reading a book at the upper end of the hall: the wheelwright was making a wheel at the lower end. Putting aside his mallet and chisel, he called to the Duke and asked him what book he was reading. 'One that records the words of the Sages.' answered the Duke. 'Are those Sages alive?` asked the wheelwright. 'Oh, no.` said the Duke. 'they are dead. ''In that case.` said the wheelwright. 'what you are reading can be nothing but the lees and scum of bygone men. `'How dare you, a wheelwright, find fault with the book I am reading. IT you can explain your statement. I will let it pass. IT not, you shall die. 'Speaking as a wheelwright.' he replied.' l look at the matter in this way: when I am making a wheel, if my stroke is too slow, then it bites deep but is not steady: if my stroke is too fast, then it is steady. but it does not go deep. The right pace, neither slow nor fast. cannot get into the hand unless it comes from the heart. It is a thing that cannot be put into words [rules]: there is an art in it that I cannot explain to my son. That is why it is impossible for me to let him take over my work, and here I am at the age of seventy still making wheels. In my opinion it must have been the same with the men of old. All that was worth handing on. died with them: the rest, they put in their books. That is why I said that what you were reading was the lees and scum of bygone men.' Chuang Tzu.

Now, what concerns us are the differences between these two sorts of knowledge; and the important differences are those which manifest themselves in the divergent ways in which these sorts of knowledge can be expressed and in the divergent ways in which they can be learned or acquired.

Technical knowledge, we have seen, is susceptible of formulation in rules, principles, directions, maxims—comprehensively, in propositions.
It is possible to write down technical knowledge in a book.
Consequently, it does not surprise us that when an artist writes about his art, he writes only about the technique of his art.
This is so, not because he is ignorant of what may be called aesthetic element, or thinks it unimportant, but because what he has to say about that he has said already (if he is a painter) in his pictures, and he knows no other way of saying it.
And the same is true when a religious man writes about his religion [8]_; or a cook about cookery.

.. [8] St. Francois de Sales was a devout man, but when he writes it is about the technique of piety.

And it may be observed that this character of being susceptible of precise formulation gives to technical knowledge at least the appearance of certainty: it appears to be possible to be certain about a technique.
On the other hand, it is a characteristic of practical knowledge that it is not susceptible of formulation of this kind.
Its normal expression is in a customary or traditional way of doing things, or, simply, in practice.
And this gives it the appearance of imprecision and consequently of uncertainty, of being a matter of opinion, of probability rather than truth.
It is, indeed, a knowledge that is expressed in taste or connoisseurship, lacking rigidity and ready for the impress of the mind of the learner.

Technical knowledge can be learned from a book; it can be learned in a correspondence course.
Moreover, much of it can be learned by heart, repeated by rote, and applied mechanically: the logic of the syllogism is a technique of this kind.
Technical knowledge, in short, can be both taught and learned in the simplest meanings of these words.
On the other hand, practical knowledge can neither be taught nor learned, but only imparted and acquired.
It exists only in practice, and the only way to acquire it is by apprenticeship to a master—not because the master can teach it (he cannot), but because it can be acquired only by continuous contact with one who is perpetually practising it.
In the arts and in natural science what normally happens is that the pupil, in being taught and in learning the technique from his master, discovers himself to have acquired also another sort of knowledge than merely technical knowledge, without it ever having been precisely imparted and often without being able to say precisely what it is.
Thus a pianist acquires artistry as well as technique, a chess-player style and insight into the game as well as a knowledge of the moves, and a scientist acquires (among other things) the sort of judgment which tells him when his technique is leading him astray and the connoisseurship which enables him to distinguish the profitable from the unprofitable directions to explore.

Now, as I understand it, Rationalism is the assertion that what I have called practical knowledge is not knowledge at all, the assertion that, properly speaking, there is no knowledge which is not technical knowledge.
The Rationalist holds that the only element of knowledge involved in any human activity is technical knowledge, and that what I have called practical knowledge is really only a sort of nescience which would be negligible if it were not positively mischievous.
The sovereignty of reason: for the Rationalist, means the sovereignty of technique.

The heart of the matter is the pre-occupation of the Rationalist with certainty.
Technique and certainty are, for him, inseparably joined because certain knowledge is, for him, knowledge which does not require to look beyond itself for its certainty; knowledge, that is, which not only ends with certainty but begins with certainty and is certain throughout.
And this is precisely what technical knowledge appears to be.
It seems to be a self-complete sort of knowledge because it seems to range between an identifiable initial point (where it breaks in upon sheer ignorance) and an identifiable terminal point, where it is complete, as in learning the rules of a new game.
It has the aspect of knowledge that can be contained wholly between the two covers of a book, whose application is, as nearly as possible, purely mechanical, and which does not assume a knowledge not itself provided in the technique.
For example, the superiority of an ideology over a tradition of thought lies in its appearance of being self-contained It can be taught best to those whose minds are empty; and if it is to be taught to one who already believes something, the first step of the teacher must be to administer a purge, to make certain that all prejudices and preconceptions are removed, to lay his foundation upon the unshakable rock of absolute ignorance.
In short, technical knowledge appears to be the only kind of knowledge which satisfies the standard of certainty which the Rationalist has chosen.

Now, I have suggested that the knowledge involved in every concrete activity is never solely technical knowledge.
If this is true, it would appear that the error of the Rationalist is of a simple sort—the error of mistaking a part for the whole, of endowing a part with the qualities of the whole.
But the error of the Rationalist does not stop there.
If his great illusion is the sovereignty of technique, he is no less deceived by the apparent certainty of technical knowledge.
The superiority of technical knowledge lay in its appearance of springing from pure ignorance and ending in certain and complete knowledge, its appearance of both beginning and ending with certainty.
But, in fact, this in an illusion.
As with every other sort of knowledge, learning a technique does not consist in getting rid of pure ignorance, but in reforming knowledge which is already there.
Nothing, not even the most nearly self-contained technique (the rules of a game), can in fact be imparted to an empty mind; and what is imparted is nourished by what is already there.
A man who knows the rules of one game will, on this account, rapidly learn the rules of another game; and a man altogether unfamiliar with 'rules' of any kind (if such can be imagined) would be a most unpromising pupil.
And just as the self-made man is never literally self-made, but depends upon a certain kind of society and upon a large unrecognized inheritance, so technical knowledge is never, in fact, self-complete, and can be made to appear so only if we forget the hypotheses with which it begins.
And if its self-completeness is illusory, the certainty which was attributed to it on account of its self-completeness is also an illusion.

But my object is not to refute Rationalism; its errors are interesting only in so far as they reveal its character.
We are considering not merely the truth of a doctrine, but the significance of an intellectual fashion in the history of post-Renaissance Europe.
And the questions we must try to answer are: What is the generation of this belief in the sovereignty of technique? Whence springs this supreme confidence in human 'reason' thus interpreted? What is the provenance, the context of this intellectual character? And in what circumstances and with what effect did it come to invade European politics?


THREE
-----

The appearance of a new intellectual character is like the appearance of a new architectural style; it emerges almost imperceptibly, under the pressure of a great variety of influences, and it is a misdirection of inquiry to seek its origins.
Indeed, there are no origins; all that can be discerned are the slowly mediated changes, the shuffling and reshuffling, the flow and ebb of the tides of inspiration, which issue finally in a shape identifiably new.
The ambition of the historian is to escape that gross abridgment of the process which gives the new shape a too early or too late and a too precise definition, and to avoid the false emphasis which springs from being over-impressed by the moment of unmistakable emergence.
Yet that moment must have a dominating interest for those whose ambitions are not pitched so high.
And I propose to foreshorten my account of the emergence of modern Rationalism, the intellectual character and disposition of the Rationalist, by beginning it at the moment when it shows itself unmistakably, and by considering only one element in the context of its emergence.
This moment is the early seventeenth century, and it was connected, inter alia, with the condition of knowledge—knowledge of both the natural and the civilized world—at that time.

The state of European knowledge at the beginning of the seventeenth century was peculiar.
Remarkable advances had already been achieved, the tide of inquiry flowed as strongly as at any other period in our history, and the fruitfulness of the presuppositions which inspired this inquiry showed no sign of exhaustion.
And yet to intelligent observers it appeared that something of supreme importance was lacking.
'The state of knowledge,' wrote Bacon, 'is not prosperous nor greatly advancing.
[9]_ And this want of prosperity was not attributable to the survival of a disposition of mind hostile to the sort of inquiry that was on foot: it was observed as a hindrance suffered by minds already fully emancipated from the presuppositions (though not, of course, from some of the details) of Aristotelian science.
What appeared to be lacking was not inspiration or even methodical habits of inquiry, but a consciously formulated technique of research, an art of interpretation, a method whose rules had been written down.
And the project of making good this want was the occasion of the unmistakable emergence of the new intellectual character I have called the Rationalist.

.. [9] Bacon. *Novum Organum* (Fowler). p. 157.

The dominating figures in the early history of this project are, of course, Bacon and Descartes, and we may find in their writings intimations of what later became the Rationalist character.

Bacon's ambition was to equip the intellect with what appeared to him necessary if certain and demonstrable knowledge of the world in which we live is to be attained.
Such knowledge is not possible for 'natural reason', which is capable of only 'petty and probable conjectures', not of certainty.
[10]_ And this imperfection is reflected in the want of prosperity of the state of knowledge.
The *Novum Organum* begins with a diagnosis of the intellectual situation.
What is lacking is a clear perception of the nature of certainty and an adequate means of achieving it.
'There remains,' says Bacon, 'but one course for the recovery of a sound and healthy condition-namely, that the entire work of understanding be commenced afresh, and the mind itself be from the very outset not left to take its own course, but guided at every step.
[11]_ What is required is a 'sure plan', a new 'way'' of understanding, an 'art' or 'method' of inquiry, an 'instrument' which (like the mechanical aids men use to increase the effectiveness of their natural strength) shall supplement the weakness of the natural reason: in short, what is required is a formulated technique of inquiry.
[12]_ He recognizes that this technique will appear as a kind of hindrance to the natural reason, not supplying it with wings but hanging weights upon it in order to control its exuberance; [13]_ but it will be a hindrance of hindrances to certainty, because it is lack of discipline which stands between the natural reason and certain knowledge of the world.
And Bacon compares this technique of research with the technique of the syllogism, the one being appropriate to the discovery of the truth of things while the other is appropriate only to the discovery of the truth of opinions.
[14]_

.. [10] Ibid. p. 184.



.. [11] Ibid. p. 182



.. [12] Ibid. p. 157.



.. [13] Ibid. p. 295.



.. [14] Ibid. p. 168.

The art of research which Bacon recommends has three main characteristics.
First, it is a set of rules; it is a true technique in that it can be formulated as a precise set of directions which can be learned by heart.
[15]_ Secondly, it is a set of rules whose application is purely mechanical; it is a true technique because it does not require for its use any knowledge or intelligence not given in the technique itself.
Bacon is explicit on this point.
The business of interpreting nature is 'to be done as if by machinery', [16]_ 'the strength and excellence of the wit (of the inquirer) has little to do with the matter', [17]_ the new method 'places all wits and understandings nearly on a level'.
[18]_ Thirdly, it is a set of rules of universal application; it is a true technique in that it is an instrument of inquiry indifferent to the subject-matter of the inquiry.

.. [15] Ibid. p. 168.



.. [16] lbid. p. 182.



.. [17] lbid. p. 162.



.. [18] lbid. p. 233.

Now, what is significant in this project is not the precise character of the rules of inquiry, both positive and negative, but the notion that a technique of this sort is even possible.
For what is proposed—infallible rules of discovery—is something very remarkable, a sort of philosopher's stone, a key to open all doors, a 'master science'.
Bacon is humble enough about the details of this method, he does not think he has given it a final formulation; but his belief in the possibility of such a 'method' in general is unbounded.
[19]_ From our point of view, the first of his rules is the most important, the precept that we must lay aside received opinion, that we must 'begin anew from the very foundations'.
[20]_ Genuine knowledge must begin with a purge of the mind, because it must begin as well as end in certainty and must be complete in itself.
Knowledge and opinion are separated absolutely: there is no question of ever winning true knowledge out of 'the childish notions we at first imbibed'.
And this, it may be remarked, is what distinguishes both Platonic and Scholastic from modern Rationalism: Plate is a rationalist, but the dialectic is not a technique, and the method of Scholasticism always had before it a limited aim.

.. [19] lbid. p. 331.



.. [20] lbid. p. 295.

The doctrine of the *Novum Organum* may be summed up, from our point of view, as the sovereignty of technique.
It represents, not merely a preoccupation with technique combined with a recognition that technical knowledge is never the whole of knowledge, but the assertion that technique and some material for it to work upon are all that matters.
Nevertheless, this is not itself the beginning of the new intellectual fashion, it is only an early and unmistakable intimation of it: the fashion itself may be said to have sprung from the exaggeration of Bacon's hopes rather than from the character of his beliefs.

Descartes, like Bacon, derived inspiration from what appeared to be the defects of contemporary inquiry; he also perceived the lack of a consciously and precisely formulated technique of inquiry.
And the method propounded in the *Discours de la Methode* and the *Regula**e* corresponds closely to that of the *Novum Organum* . For Descartes, no less than for Bacon, the aim is certainty.
Certain knowledge can spring up only in an emptied mind; the technique of research begins with an intellectual purge.
The first principle of Descartes is 'de ne recevoir jamais aucune chose pour vraie que je ne la connusse evidemment etre telle, c'est-a-dire d'eviter soigneusement la precipitation et la prevention','de batir dans un fonds qui est tout a moi'; and the inquirer is said to be 'comme un homme qui marche seul et dans les tenebres'.
[21]_ Further, the technique of inquiry is formulated in a set of rules which, ideally, compose an infallible method whose application is mechanical and universal.
And thirdly, there are no grades in knowledge, what is not certain is mere nescience.
Descartes, however, is distinguished from Bacon in respect of the thoroughness of his education in the Scholastic philosophy and in the profound impression that geometrical demonstration had upon his mind, and the effect of these differences in education and inspiration is to make his formulation of the technique of inquiry more precise and in consequence more critical.

.. [21] *Discours de la Methode*, ii.

His mind is oriented towards the project of an infallible and universal method or research, but since the method he propounds is modelled on that of geometry, its limitation when applied, not to possibilities but to things, is easily apparent.
Descartes is more thorough than Bacon in doing his scepticism for himself and, in the end, he recognizes it to be an error to suppose that the method can ever be the sole means of inquiry.
[22]_ The sovereignty of technique turns out to be a dream and not a reality.
Nevertheless, the lesson his successors believed themselves to have learned from Descartes was the sovereignty of technique and not his doubtfulness about the possibility of an infallible method.

.. [22] lbid. vi.

By a pardonable abridgment of history, the Rationalist character may be seen springing from the exaggeration of Bacon's hopes and the neglect of the scepticism of Descartes; modern Rationalism is what commonplace minds made out of the inspiration of men of discrimination and genius.
Les grands hommes, en apprenant auxfaibles a reflechir, les ont mis sur la route de I'erreur.
But the history of Rationalism is not only the history of the gradual emergence and definition of this new intellectual character; it is, also, the history of the invasion of every department of intellectual activity by the doctrine of the sovereignty of technique.
Descartes never became a Cartesian; but, as Bouillier says of the seventeenth century, 'le cartesianisme a triomphe; il s'est empare du grand siecle tout entier, il a penetre de son esprit, non seulement la philosophie, mais les sciences et les lettres ellesmemes'.
[23]_ It is common knowledge that, at this time, in poetry and in drama, there was a remarkable concentration on technique, on rules of composition, on the observance of the bienseances of literature, which continued unabated for nearly two centuries.
A stream of books flowed from the presses on the 'art of poetry', the 'art of living', the 'art of thinking'.
Neither religion, nor natural science, nor education, nor the conduct of life itself escaped from the influence of the new Rationalism; no activity was immune, no society untouched.
[24]_

.. [23] *Histoire de la philosophie cartesienne*, i. 486



.. [24] One important aspect of the history of the emergence of Rationalism is the changing connotation of the word 'reason`. The 'reason' to which the Rationalist appeals is not, for example, the Reason of Hooker. which belongs still to the tradition of Stoicism and of Aquinas. It is a faculty of calculation by which men conclude one thing from another and discover fit means of attaining given ends not themselves subject to the criticism of reason, a faculty by which a world believed to be a machine could be disclosed. Much of the plausibility of Rationalism lies in the tacit attribution to the new reason` of the qualities which belong properly to the Reason of the older intellectual tradition. And this ambiguity, the emergence of the new connotation out of the old, may be observed in many of the writers of the early seventeenth century—in. for example, the poetry of Malherbe, an older contemporary of Descartes. and one of the great progenitors of the sovereignty of technique in literature

The slowly mediated changes by which the Rationalist of the seventeenth century became the Rationalist as we know him today, are a long and complicated story which I do not propose even to abridge.
It is important only to observe that, with every step it has taken away from the true sources of its inspiration, the Rationalist character has become cruder and more vulgar.
What in the seventeenth century was 'L'art de penser' has now become Your mind and how to use it, a plan by world-famous experts for developing a trained mind at a fraction of the usual cost.
What was the Art of Living has become the Technique of Success, and the early and more modest incursions of the sovereignty of technique into education have blossomed into Pelmanism.

The deeper motivations which encouraged and developed this intellectual fashion are, not unnaturally, obscure; they are hidden in the recesses of European society.
But among its other connections, it is certainly closely allied with a decline in the belief in Providence: a beneficient and infallible technique replaced a beneficient and infallible God; and where Providence was not available to correct the mistakes of men it was all the more necessary to prevent such mistakes.
Certainly, also, its provenance is a society or a generation which thinks what it has discovered for itself is more important than what it has inherited, [25]_ an age over-impressed with its own accomplishment and liable to those illusions of intellectual grandeur which are the characteristic lunacy of post-Renaissance Europe, an age never mentally at peace with itself because never reconciled with its past.
And the vision of a technique which puts all minds on the same level provided just the short-cut which would attract men in a hurry to appear educated but incapable of appreciating the concrete detail of their total inheritance.

.. [25] This was certainly true of the age of Bacon. And Professor Bernal now tells us that more has been found out at large and in detail about nature and man in the thirty years after 1915 than in the whole of history.

And, partly under the influence of Rationalism itself, the number of such men has been steadily growing since the seventeenth century.
[26]_ Indeed it may be said that all, or almost all, the influences which in its early days served to encourage the emergence of the Rationalist character have subsequently become more influential in our civilization.

.. [26] Not so very long ago. I suppose. the spectators at horse-races were mostly men and women who knew something at first-hand about horses, and who (in this respect) were genuinely educated people. This has ceased to be so, except perhaps in Ireland. And the ignorant spectator. with no ability, inclination or opportunity to educate himself, and seeking a short-cut out of his predicament, demands a book. (The twentieth century vogue in cookery books derives, no doubt, from a similar situation.) The authors of one such book. *A Guide to the Classics, or how to pick the Derby winner*, aware of the difference between technical and complete knowledge. were at pains to point out that there was a limit beyond which there were no precise rules for picking the winner, and that some intelligence (not supplied by the rules themselves) was necessary But some of its greedy, rationalistic readers, on the look-out for an infallible method, which (like Bacon`s) would place their small wits on a level with men of genuine education, thought they had been sold a pup—which only goes to show how much better they would have spent their time if they had read St. Augustine or Hegel instead of Descartes: je ne puis pardonner a Descartes.*A Guide to the Classics. or how to pick the Derby winner* was co-authored by Oakeshott and Guy Griffith. It was first published in 1936 by Faber & Faber. It was reissued in a revised edition in 1947 under the title *A New Guide to the Derby. How to pick the winner*. —T.F.]

Now, it is not to be thought that Rationalism established itself easily and without opposition.
It was suspect as a novelty, and some fields of human activity-literature, for example—on which at first its hold was strong, subsequently freed themselves from its grasp.
Indeed, at all levels and in all fields there has been continuous criticism of the resistance to the teachings of Rationalism.
And the significance of the doctrine of the sovereignty of technique becomes clearer when we consider what one of its first and profoundest critics has to say about it.
Pascal is a judicious critic of Descartes, not opposing him at all points, but opposing him nevertheless, on points that are fundamental.
[27]_ He perceived, first, that the Cartesian desire for certain knowledge was based upon a false criterion of certainty.
Descartes must begin with something so sure that it cannot be doubted, and was led, as a consequence, to believe that all genuine knowledge is technical knowledge.

.. [27] Pensees (Brunschvicg). i. 76.

Pascal avoided this conclusion by his doctrine of probability: the only knowledge that is certain is certain on account of its partiality; the paradox that probable knowledge has more of the whole truth than certain knowledge Secondly, Pascal perceived that the Cartesian *raisonnement* is never in fact the whole source of the knowledge involved in any concrete activity.
The human mind, he asserts, is not wholly dependent for its successful working upon a conscious and formulated technique; and even where a technique is involved, the mind observes the technique 'tacitement, naturellement et sans art'.
The precise formulation of rules of inquiry endangers the success of the inquiry by exaggerating the importance of method.
Pascal was followed by others, and indeed much of the history of modern philosophy revolves round this question.
But, though later writers were often more elaborate in their criticism, few detected more surely than Pascal that the significance of Rationalism is not its recognition of technical knowledge, but its failure to recognize any other: its philosophical error lies in the certainty it attributes to technique and in its doctrine of the sovereignty of technique; its practical error lies in its belief that nothing but benefit can come from making conduct self-conscious.


FOUR
----

It was, of course, improbable that politics should altogether escape the impress of so strong and energetic an intellectual style as that of the new Rationalism.
But what, at first sight, is remarkable is that politics should have been earlier and more fully engulfed by the tidal wave than any other human activity.
The hold of Rationalism upon most departments of life has varied in its firmness during the last four centuries but in politics it has steadily increased and is Stronger now than at any earlier time.
We have considered already the general intellectual disposition of the Rationalist when he turns to politics: what remains to be considered are the circumstances in which European politics came to surrender almost completely to the Rationalist and the results of the surrender.

That all contemporary politics are deeply infected with Rationalism will be denied only by those who choose to give the infection another name.
Not only are our political vices rationalistic, but so also are our political virtues.
Our projects are, in the main, rationalist in purpose and character; but, what is more significant, our whole attitude of mind in politics is similarly determined.
And those traditional elements, particularly in English politics, which might have been expected to continue some resistance to the pressure of Rationalism, have now almost completely conformed to the prevailing intellectual temper, and even represent this conformity to be a sign of their vitality, their ability to move with the times.
Rationalism has ceased to be merely one style in politics and has become the stylistic criterion of all respectable politics.

How deeply the rationalist disposition of mind has invaded our political thought and practice is illustrated by the extent to which traditions of behaviour have given place to ideologies, the extent to which the politics of destruction and creation have been substituted for the politics of repair, the consciously planned and deliberately executed being considered (for that reason) better than what has grown up and established itself unselfconsciously over a period of time.
This conversion of habits of behaviour, adaptable and never quite fixed or finished, into comparatively rigid systems of abstract ideas, is not, of course, new; so far as England is concerned it was begun in the seventeenth century, in the dawn of rationalist politics.
But, while formerly it was tacitly resisted and retarded by, for example, the informality of English politics (which enabled us to escape, for a long time, putting too high a value on political action and placing too high a hope in political achievement—to escape, in politics at least, the illusion of the evanescence of imperfection), that resistance has now itself been converted into an ideology.
[28]_ This is, perhaps, the main significance of Hayek's *Road to Serfdom*—not the cogency of his doctrine, but the fact that it is a doctrine, A plan to resist all planning may be better than its opposite, but it belongs to the same style of politics.

.. [28] A tentative. and therefore not a fundamentally damaging, conversion of this sort was attempted by the first Lord Halifax.

And only in a society already deeply infected with Rationalism will the conversion of the traditional resources of resistance to the tyranny of Rationalism into a self-conscious ideology be considered a strengthening of those resources.
It seems that now, in order to participate in politics and expect a hearing, it is necessary to have, in the strict sense, a doctrine; not to have a doctrine appears frivolous, even disreputable.
And the sanctity, which in some societies was the property of a politics piously attached to traditional ways, has now come to belong exclusively to rationalist politics.

Rationalist politics, I have said, are the politics of the felt need, the felt need not qualified by a genuine, concrete knowledge of the permanent interests and direction of movement of a society, but interpreted by 'reason' and satisfied according to the technique of an ideology: they are the politics of the book.
And this also is characteristic of almost all contemporary politics: not to have a book is to be without the one thing necessary, and not to observe meticulously what is written in the book is to be a disreputable politician.
Indeed, so necessary is it to have a book, that those who have hitherto thought it possible to get on without one, have had, rather late in the day, to set about composing one for their own use.
This is a symptom of the triumph of technique which we have seen to be the root of modern Rationalism; for what the book contains is only what it is possible to put into a book—rules of a technique.
And, book in hand (because, though a technique can be learned by rote, they have not always learned their lesson well), the politicians of Europe pore over the simmering banquet they are preparing for the future; but, like jumped-up kitchen-porters deputizing for an absent cook, their knowledge does not extend beyond the written word which they read mechanically—it generates ideas in their heads but no tastes in their mouths.

Among the other evidences of Rationalism in contemporary politics, may be counted the commonly admitted claim of the 'scientist' as such the chemist, the physicist, the economist or the psychologist) to be heard in politics; because, though the knowledge involved in a science is always more than technical knowledge, what it has to offer to politics is never more than a technique.
And under this influence, the intellect in politics ceases to be the critic Of political habit and becomes a substitute for habit, and the life of a society loses its rhythm and continuity and is resolved into a succession of problems and crises.
Folk-lore, because it is not technique, is identified with nescience, and all sense of what Burke called the partnership between present and past is lost.
[29]_

.. [29] A poetic image of the politics of Rationalism is to be found in Rex Warner`s book. *The Aerodrome* .

There is, however, no need to labour the point that the most characteristic thing about contemporary politics is their rationalist inspiration; the prevailing belief that politics are easy is, by itself, evidence enough.
And if a precise example is required we need look no further for it than the proposals we have been offered for the control of the manufacture and use of atomic energy.
The rationalist faith in the sovereignty of technique is the presupposition both of the notion that some over-all scheme of mechanized control is possible and of the details of every scheme that has so far been projected: it is understood as what is called an 'administrative' problem.
But, if Rationalism now reigns almost unopposed, the question which concerns us is, What are the circumstances that promoted this state of affairs? For the significance of the triumph lies not merely in itself, but in its context.

Briefly, the answer to this question is that the politics of Rationalism are the politics of the politically inexperienced, and that the outstanding characteristic of European politics in the last four centuries is that they have suffered the incursion of at least three types of political inexperience—that of the new ruler, of the new ruling class, and of the new political society—to say nothing of the incursion of a new sex, lately provided for by Mr.
Shaw.
How appropriate rationalist politics are to the man who, not brought up or educated to their exercise, finds himself in a position to exert political initiative and authority, requires no emphasis.
His need of it is so great that he will have no incentive to be sceptical about the possibility of a magic technique of politics which will remove the handicap of his lack of political education.

The offer of such a technique will seem to him the offer of salvation itself; to be told that the necessary knowledge is to be found, complete and self-contained, in a book, and to be told that this knowledge is of a sort that can be learned by heart quickly and applied mechanically, will seem, like salvation, something almost too good to be true.
And yet it was this, or something near enough to be mistaken for it, which he understood Bacon and Descartes to be offering him.
For, though neither of these writers ventures upon the detailed application of his method to politics, the intimations of rationalist politics are present in both, qualified only by a scepticism which could easily be ignored.
Nor had he to wait for Bacon and Descartes (to wait, that is, for a general doctrine of Rationalism); the first of these needy adventurers into the field of politics was provided for on his appearance a century earlier by Machiavelli.

It has been said that the project of Machiavelli was to expound a science of politics, but this, I think, misses the significant point.
A science, we have seen, is concrete knowledge and consequently neither its conclusions, nor the means by which they were reached, can ever, as a whole, be written down in a book.
Neither an art nor a science can be imparted in a set of directions; to acquire a mastery in either is to acquire an appropriate connoisseurship.
But what can be imparted in this way is a technique, and it is with the technique of politics that Machiavelli, as a writer, is concerned.
He recognized that the technique of governing a republic was somewhat different from that appropriate to a principality, and he was concerned with both.
But in writing about the government of Principalities he wrote for the new prince of his day, and this for two reasons, one of principle and the other personal.
The well-established hereditary ruler, educated in a tradition and heir to a long family experience, seemed to be well enough equipped for the position he occupied: his politics might be improved by a correspondence course in technique, but in general he knew how to behave.
But with the new ruler, who brought to his task only the qualities which had enabled him to gain political power and who learnt nothing easily but the vices of his office, the caprice de prince, the position was different.
Lacking education (except in the habits of ambition), and requiring some short-cut to the appearance of education, he required a book.
But he required a book of a certain sort; he needed a crib: his inexperience prevented him from tackling the affairs of State unseen.
Now, the character of a crib is that its author must have an educated man's knowledge of the language, that he must prostitute his genius (if he has any) as a translator, and that it is powerless to save the ignorant reader from all possibility of mistake.
The project of Machiavelli was, then, to provide a crib to politics, a political training in default of a political education, a technique for the ruler who had no tradition.
He supplied a demand of his time; and he was personally and temperamentally interested in supplying the demand because he felt the 'fascination of what is difficult'.
The new ruler was more interesting because he was far more likely than the educated hereditary ruler to get himself into a tricky situation and to need the help of advice.
But, like the great progenitors of Rationalism in general (Bacon and Descartes), Machiavelli was aware of the limitations of technical knowledge; it was not Machaivelli himself, but his followers, who believed in the sovereignty of technique, who believed that government was nothing more than 'public administration' and could be learned from a book.
And to the new prince he offered not only his book, but also, what would make up for the inevitable deficiencies of his book—himself: he never lost the sense that politics, after all, are diplomacy, not the application of a technique.

The new and politically inexperienced social classes which, during the last four centuries, have risen to the exercise of political initiative and authority, have been provided for in the same sort of way as Machiavelli provided for the new prince of the sixteenth century.
None of these classes had time to acquire a political education before it came to power: each needed a crib, a political doctrine, to take the place of a habit of political behaviour.
Some of these writings are genuine works of political vulgarization: they do not altogether deny the existence or worth of a political tradition (they are written by men of real political education), but they are abridgments of a tradition, rationalizations purporting to elicit the 'truth' of a tradition and to exhibit it in a set of abstract principles, but from which, nevertheless, the full significance of the tradition inevitably escapes.
This is pre-eminently so of Locke's *Second Treatise of Civil Government* , which was as popular, as long-lived and as valuable a political crib as that greatest of all cribs to a religion, Paley's *Evidences of Christianity* . But there are other writers, like Bentham or Godwin, who, pursuing the common project of providing for the political inexperience of succeeding generations, cover up all trace of the political habit and tradition of their society with a purely speculative idea: these belong to the strictest sect of Rationalism.
But, so far as authority is concerned, nothing in this field can compare with the work of Marx and Engels.
European politics without these writers would still have been deeply involved in Rationalism, but beyond question they are the authors of the most stupendous of our political rationalisms—as well they might be, for it was composed for the instruction of a less politically educated class than any other that has ever come to have the illusion of exercising political power.
And no fault can be found with the mechanical manner in which this greatest of all political cribs has been learned and used by those for whom it was written.
No other technique has so imposed itself upon the world as if it were concrete knowledge; none has created so vast an intellectual proletariat with nothing but its technique to lose.
[30]_

.. [30] By casting his technique in the form of a view of the course of events (past. present and future), and not of human nature'. Marx thought he had escaped from Rationalism: but since he had taken the precaution of first turning the course of events into a doctrine, the escape was an illusion. Like Midas. the Rationalist is always in the unfortunate position of not being able to touch anything, without transforming it into an abstraction: he can never get a square meal of experience.

The early history of the United States of America is an instructive chapter in the history of the politics of Rationalism.
The situation of a society called upon without much notice to exercise political initiative on its own account is similar to that of an individual or a social class rising not fully prepared to the exercise of political power; in general, its needs are the same as theirs.
And the similarity is even closer when the independence of the society concerned begins with an admitted illegality, a specific and express rejection of a tradition.
which consequently can be defended only by an appeal to something which is itself thought not to depend upon tradition.
Nor, in the case of the American colonists, was this the whole Of the pressure which forced their revolution into the pattern of Rationalism.

The founders of American independence had both a tradition of European thought and a native political habit and experience to draw upon.
But, as it happened, the intellectual gifts of Europe to America (both in philosophy and religion) had, from the beginning, been predominantly rationalistic: and the native political habit, the product of the circumstances of colonisation, was what may be called a kind of natural and unsophisticated rationalism.
A plain and unpretending people, not given over-much to reflection upon the habits of behaviour they had in fact inherited, who, in frontier communities, had constantly the experience of setting up law and order for themselves by mutual agreement, were not likely to think of their arrangements except as the creation of their own unaided initiative; they seemed to begin with nothing, and to owe to themselves all that they had come to possess.
A civilization of pioneers is, almost unavoidably, a civilization of self-consciously self-made men, Rationalists by circumstance and not by reflection, who need no persuasion that knowledge begins with a tabula rasa and who regard the free mind, not even as the result of some artificial Cartesian purge, but as the gift of Almighty God, as Jefferson said.

Long before the Revolution, then, the disposition of mind of the American colonists, the prevailing intellectual character and habit of politics, were rationalistic.
And this is clearly reflected in the constitutional documents and history of the individual colonies.
And when these colonies came 'to dissolve the political bands which have connected them with another', and to declare their independence, the only fresh inspiration that this habit of politics received from the outside was one which confirmed its native character in every particular.
For the inspiration of Jefferson and the other founders of American independence was the ideology which Locke had distilled from the English political tradition.
They were disposed to believe, and they believed more fully than was possible for an inhabitant of the Old World, that the proper organization of a society and the conduct of its affairs were based upon abstract principles, and not upon a tradition which, as Hamilton said, had 'to be rummaged for among old parchments and musty records'.
These principles were not the product of civilization; they were natural, 'written in the whole volume of human nature'.
[31]_ They were to be discovered in nature by human reason, by a technique of inquiry available alike to all men and requiring no extraordinary intelligence in its use.
Moreover, the age had the advantage of all earlier ages because, by the application of this technique of inquiry, these abstract principles had, for the most part recently, been discovered and written down in books.
And by using these books, a newly made political society was not only not handicapped by the lack of a tradition, but had a positive superiority over older societies not yet fully emancipated from the chains of custom.
What Descartes had already perceived, 'que souvent ii n'y a pas tant de perfection dans les ouvrages composes de plusieurs pieces et faits de la main de divers maitres qu'en ceux anquels un seul a travaille', was freshly observed in 1777 by John Jay-'The Americans are the first people whom Heaven has favoured with an opportunity of deliberating upon, and choosing the forms of government under which they should live.
All other constitutions have derived their existence from violence or accidental circumstances, and are therefore probably more distant from their perfection....' [32]_ The Declaration of Independence is a characteristic product of the *saeculum rationalisticum* . It represents the politics of the felt need interpreted with the aid of an ideology.
And it is not surprising that it should have become one of the sacred documents of the politics of Rationalism, and, together with the similar documents of the French Revolution, the inspiration and pattern of many later adventures in the rationalist reconstruction of society.

.. [31] There is no spare here to elucidate the exceedingly complicated connections between the Politics of 'reason' and the politics of nature But it may be observed that, since both reason and nature were opposed to civilization. they began with a common ground: and the 'rational` man. the man freed from the idols and prejudices of a tradition, could, alternatively, be called the. natural. man. Modern Rationalism and modern Naturalism in politics, in religion and in education, are alike expressions of a general presumption against all human achievement more than about a generation old.



.. [32] Of course both .violence and 'accidental circumstances' were there, but being present in an unfamiliar form they were unrecognized.

The view I am maintaining is that the ordinary practical politics of European nations have become fixed in a vice of Rationalism, that much of their failure (which is often attributed to other and more immediate causes [33]_) springs in fact from the defects of the Rationalist character when it is in control of affairs, and that (since the rationalist disposition of mind is not a fashion which sprang up only yesterday) we must not expect a speedy release from our predicament.
It is always depressing for a patient to be told that his disease is almost as old as himself and that consequently there is no quick cure for it, but (except for the infections of childhood) this is usually the case.
So long as the circumstances which promoted the emergence of rationalist politics remain, so long must we expect our politics to be rationalist in disposition.

.. [33] War, for example. War is a disease to which a rationalist society has little resistance; it springs easily from the kind of incompetence inherent in rationalist politics. But it has certainly increased the hold of the Rationalist disposition of mind on politics, and one of the disasters of war has been the new customary application to politics of its essentially rationalist vocabulary.

I do not think that any or all of the writers whom I have mentioned are responsible for our predicament.
They are the servants of circumstances which they have helped to perpetuate (on occasion they may be observed giving another turn to the screw), but which they did not create.
And it is not to be supposed that they would always have approved of the use made of their books.
Nor, again, am I concerned with genuinely philosophical writing about politics; in so far as that has either promoted or retarded the tendency to Rationalism in politics, it has always been through a misunderstanding of its design, which is not to recommend conduct but to explain it.
To explore the relations between politics and eternity is one thing: it is something different, and less commendable, for a practical politician to find the intricacy of the world of time and contingency so unmanageable that he is bewitched by the offer of a quick escape into the bogus eternity of an ideology.
Nor, finally, do I think we owe our predicament to the place which the natural sciences and the manner of thinking connected with them has come to take in our civilization.
This simple diagnosis of the situation has been much put about, but I think it is mistaken.
That the influence of the genuine natural scientist is not necessarily on the side of Rationalism follows from the view I have taken of the character of any kind of concrete knowledge.

No doubt there are scientists deeply involved in the rationalist attitude, but they are mistaken when they think that the rationalist and the scientific points of view necessarily coincide.
The trouble is that when the scientist steps outside his own field he often carries with him only his technique, and this at once allies him with the forces of Rationalism.
[34]_ In short, I think the great prestige of the natural sciences has, in fact, been used to fasten the rationalist disposition of mind more firmly upon us, but that this is the work, not of the genuine scientist as such, but of the scientist who is a Rationalist in spite of his science.

.. [34] A celebrated scientist tells us: '1 am less interested than the average person in politics because 1 am convinced that all political principles today are makeshifts, and will ultimately be replaced by principles of scientific knowledge.


FIVE
----

To this brief sketch of the character, and the social and intellectual context of the emergence of Rationalism in politics, may be added a few reflections.
The generation of rationalist politics is by political inexperience out of political opportunity.
These conditions have often existed together in European societies; they did so in the ancient world, and that world at times suffered the effects of their union.
But the particular quality of Rationalism in modern politics derives from the circumstance that the modern world succeeded in inventing so plausible a method of covering up lack of political education that even those who suffered from that lack were often left ignorant that they lacked anything.
Of course, this inexperience was never, in any society, universal; and it was never absolute.
There have always been men of genuine political education, immune from the infection of Rationalism (and this is particularly so Of England, where a political education of some sort has been much more widely spread than in some other societies): and sometimes a dim reminder of the limitations of his technique has penetrated even the mind of the Rationalist.

Indeed, so impractical is a purely 'rationalist politics, that the new man, lately risen to power, will often be found throwing away his book and relying upon his general experience of the world as, for example, a business man or a trade union official.
This experience is certainly a more trustworthy guide than the book—at least it is real knowledge and not a shadow—but still, it is not a knowledge of the political traditions of his society, which, in the most favourable circumstances, takes two or three generations to acquire.

Nevertheless, when he is not arrogant or sanctimonious, the Rationalist can appear a not unsympathetic character.
He wants so much to be right.
But unfortunately he will never quite succeed.
He began too late and on the wrong foot.
His knowledge will never be more than half-knowledge, and consequently he will never be more than half-right.
[35]_ Like a foreigner or a man out of his social class, he is bewildered by a tradition and a habit of behaviour of which he knows only the surface; a butler or an observant house-maid has the advantage of him.
And he conceives a contempt for what he does not understand; habit and custom appear bad in themselves, a kind of nescience of behaviour.
And by some strange self-deception, he attributes to tradition (which, of course, is pre-eminently fluid) the rigidity and fixity of character which in fact belongs to ideological politics.
Consequently, the Rationalist is a dangerous and expensive character to have in control of affairs, and he does most damage, not when he fails to master the situation (his politics, of course, are always in terms of mastering situations and surmounting crises), but when he appears to be successful; for the price we pay for each of his apparent successes is a firmer hold of the intellectual fashion of Rationalism upon the whole life of society.

.. [35] There is a reminiscence here of a passage in Henry James. whose study of Mrs. Headway in *The Siege of London* is the best I know of a person in this position.

Without alarming ourselves with imaginary evils, it may, I think, be said that there are two characteristics, in particular, of political Rationalism which make it exceptionally dangerous to a society.
No sensible man will worry greatly because he cannot at once hit upon a cure for what he believes to be a crippling complaint; but if he sees the complaint to be of a kind which the passage of time must make more rather than less severe, he will have a more substantial cause for anxiety.
And this unfortunately appears to be so with the disease of Rationalism.

First, Rationalism in politics, as I have interpreted it, involves ., identifiable error, a misconception with regard to the nature of human knowledge, which amounts to a corruption of the mind.
And consequently it is without the power to correct its own shortcomings; it has no homeopathic quality; you cannot escape its errors by becoming more sincerely or more profoundly rationalistic.
This, it may be observed, is one of the penalties of living by the book; it leads not only to specific mistakes, but it also dries up the mind itself: living by precept in the end generates intellectual dishonesty.
And further, the Rationalist has rejected in advance the only external inspiration capable of correcting his error; he does not merely neglect the kind of knowledge which would save him, he begins by destroying it.
First he turns out the light and then complains that he cannot see, that he is 'comme un homme qui marche seul et dans les tenebres In short, the Rationalist is essentially ineducable: and he could be educated out of his Rationalism only by an inspiration which he regards as the great enemy of mankind.
All the Rationalist can do when left to himself is to replace one rationalist project in which he has failed by another in which he hopes to succeed.
Indeed, this is what contemporary politics are fast degenerating into: the political habit and tradition, which, not long ago, was the common possession of even extreme opponents in English politics, has been replaced by merely a common rationalist disposition of mind.

But, secondly, a society which has embraced a rationalist idiom Of politics will soon find itself either being steered or drifting towards an exclusively rationalist form of education.
I do not mean the crude purpose of National Socialism or Communism of allowing "O education except a training in the dominant rationalist doctrine, I mean the more plausible project of offering no place to any form of education which is not generally rationalistic in character.
[36]_

.. [36] Something of this sort happened in France after the Revolution: but it was not long before Sanity began to break In.

And when an exclusively rationalist form of education is fully established, the only hope of deliverance lies in the discovery by some neglected pedant, 'rummaging among old parchments and musty records', of what the world was like before the millennium overtook it.

From the earliest days of his emergence, the Rationalist has taken an ominous interest in education.
He has a respect for 'brains', a great belief in training them, and is determined that cleverness shall be encouraged and shall receive its reward of power.
But what is this education in which the Rationalist believes? It is certainly not an initiation into the moral and intellectual habits and achievements of his society, an entry into the partnership between present and past, a sharing of concrete knowledge; for the Rationalist, all this would be an education in nescience, both valueless and mischievous.
It is a training in technique, a training, that is, in the half of knowledge which can be learnt from books when they are used as cribs.
And the Rationalist's affected interest in education escapes the suspicion of being a mere subterfuge for imposing himself more firmly on society, only because it is clear that he is as deluded as his pupils.
He sincerely believes that a training in technical knowledge is the only education worth while, because he is moved by the faith that there is no knowledge, in the proper sense, except technical knowledge.
He believes that a training in 'public administration' is the surest defence against the flattery of a demagogue and the lies of a dictator.

Now, in a society already largely rationalist in disposition, there will be a positive demand for training of this sort.
Half-knowledge (SO long as it is the technical half) will have an economic value; there will be a market for the 'trained' mind which has at its disposal the latest devices.
And it is only to be expected that this demand will be satisfied; books of the appropriate sort will be written and sold in large quantities, and institutions offering a training of this kind (either generally or in respect of a particular activity) will spring up.
[37]_

.. [37] Some people regard this as the inevitable result of an industrial civilization, but I think they have hit upon the wrong culprit. What an industrial civilization needs is genuine skill: and in so far as our industrial civilization has decided to dispense with skill and to get along with merely technical knowledge It is an industrial civilization gone to the bad.

And so far as our society is concerned, it is now long since the exploitation of this demand began in earnest; it was already to be observed in the early nineteenth century.
But it is not very important that people should learn the piano or how to manage a farm by a correspondence course; and in any case it is unavoidable in the circumstances.
What is important, however, is that the rationalist inspiration has now invaded and has begun to corrupt the genuine educational provisions and institutions of our society: some of the ways and means by which, hitherto, a genuine (as distinct from a merely technical] knowledge has been imparted have already disappeared, others are obsolescent, and others again are in process of being corrupted from the inside.
The whole pressure of the circumstances of our time is in this direction.
Apprenticeship, the pupil working alongside the master who in teaching a technique also imparts the sort of knowledge that cannot be taught, has not yet disappeared; but it is obsolescent, and its place is being taken by technical schools whose training (because it can be a training only in technique) remains insoluble until it is immersed in the acid of practice.
Again, professional education is coming more and more to be regarded as the acquisition of a technique, [38]_ something that can be done through the post, with the result that we may look forward to a time when the professions will be stocked with clever men, but men whose skill is limited and who have never had a proper opportunity of learning the nuances which compose the tradition and standard of behaviour which belong to a great profession.
[39]_ One of the ways in which this sort of knowledge has hitherto been preserved (because it is a great human achievement, and if it is not positively preserved it will be lost) and transmitted is a family tradition.
But the Rationalist never understands that it takes about two generations of practice to learn a profession; indeed, he does everything he can to destroy the possibility of such an education, believing it to be mischievous.

.. [38] Cf. James Boswell. *The Artists Dilemma*.



.. [39] The army in wartime was a particularly good opportunity of observing the difference between a trained and an educated man: the intelligent civilian had little difficulty in acquiring the technique of military leadership and command, but (in spite of the cribs provided: *Advice to Young Officers* . etc.) he always remained at a disadvantage beside the regular officer, the man educated in the feelings and emotions as well as the practices of his profession.

Like a man whose only language is Esperanto, he has no means of knowing that the world did not begin in the twentieth century.
And the priceless treasure of great professional traditions is, not negligently but purposefully, destroyed in the destruction of so-called vested interests.
But perhaps the most serious rationalist attack upon education is that directed against the Universities.
The demand for technicians is now so great that the existing institutions for training them have become insufficient, and the Universities are in process of being procured to satisfy the demand.
The ominous phrase, 'university trained men and women', is establishing itself, and not only in the vocabulary of the Ministry of Education.

To an opponent of Rationalism these are local, though not negligible, defeats, and, taken separately, the loss incurred in each may not be irreparable.
At least an institution like a University has a positive power of defending itself, if it will use it.
But there is a victory which the Rationalist has already won on another front from which recovery will be more difficult because, while the Rationalist knows it to be a victory, his opponent hardly recognizes it as a defeat.
I mean the circumvention and appropriation by the rationalist disposition of mind of the whole field of morality and moral education.
The morality of the Rationalist is the morality of the self-conscious pursuit of moral ideals, and the appropriate form of moral education is by precept, by the presentation and explanation of moral principles.
This is presented as a higher morality (the morality of the free man: there is no end to the clap-trap) than that of habit, the unselfconscious following of a tradition of moral behaviour; but, in fact, it is merely morality reduced to a technique, to be acquired by training in an ideology rather than an education in behaviour.
In morality, as in everything else, the Rationalist aims to begin by getting rid of inherited nescience and then to fill the blank nothingness of an open mind with the items of certain knowledge which he abstracts from his personal experience, and which he believes to be approved by the common 'reason' of mankind.
[40]_

.. [40] Of this, and other excesses of Rationalism. Descartes himself was not guilty. *Discours de la Methode*. iii.

He will defend these principles by argument, and they will compose a coherent [though morally parsimonious) doctrine.
But, unavoidably, the conduct of life, for him, is a jerky, discontinuous affair, the solution of a stream of problems, the mastery of a succession of crises.
Like the politics of the Rationalist (from which, of course, it is inseparable), the morality of the Rationalist is the morality of the self-made man and of the self-made society: it is what other peoples have recognized as 'idolatry'.
And it is of no consequence that the moral ideology which inspires him today (and which, if he is a politician, he preaches] is, in fact, the desiccated relic of what was once the unselfconscious moral tradition of an aristocracy who, ignorant of ideals, had acquired a habit of behaviour in relation to one another and had handed it on in a true moral education.
For the Rationalist, all that matters is that he has at last separated the ore of the ideal from the dress of the habit of behaviour; and, for us, the deplorable consequences of his success.
Moral ideals are a sediment; they have significance only so long as they are suspended in a religious or social tradition, so long as they belong to a religious or a social life.
[41]_ The predicament of our time is that the Rationalists have been at work so long on their project of drawing off the liquid in which our moral ideals were suspended (and pouring it away as worthless) that we are left only with the dry and gritty residue which chokes us as we try to take it down.
First, we do our best to destroy parental authority [because of its alleged abuse], then we sentimentally deplore the scarcity of 'good homes', and we end by creating substitutes which complete the work of destruction.

.. [41] When Confucius visited Lao Tzu he talked of goodness and duty. .Chaff from the winnower's fan.' said Lao Tzu. 'can so blear the eyes that we do not know if we are looking north, South. east or west: at heaven or at earth... All this talk of goodness and duty, these Perpetual pin-pricks, unnerve and irritate the hearer: nothing. indeed. could be more destructive of inner tranquillity.' *Chuang Tzu .*

And it is for this reason that, among much else that is corrupt and unhealthy, we have the spectacle of a set of sanctimonious, rationalist politicians, preaching an ideology of unselfishness and social service to a population in which they and their predecessors have done their best to destroy the only living root of moral behaviour; and opposed by another set of politicians dabbling with the project of converting us from Rationalism under the inspiration of a fresh rationalization of our political tradition.

odt2rst
=======

The goal is to be able to convert Open Document produced with Open
Office (.odt files) into reStructuredText file (.rst files).

In particular to convert back the .odt files produced by rst2odt into
.rst files.

To let people review and modify .rst file using OpenOffice.

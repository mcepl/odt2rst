#!/usr/bin/env python3
import unittest

from distutils.core import Command, setup


class RunTests(Command):
    """New setup.py command to run all tests for the package.
    """
    description = "run all tests for the package"

    user_options = []

    def initialize_options(self):
        pass

    def finalize_options(self):
        pass

    def run(self):
        tests = unittest.TestLoader().discover(".")
        runner = unittest.TextTestRunner(verbosity=2)
        runner.run(tests)


setup(name="odt2rst",
      version="2.0",
      description="convert odt to rst.",
      author="Vivian De Smedt",
      author_email="vivian@vdesmedt.com",
      cmdclass={'test': RunTests},
      scripts=["odt2rst.py"],
      classifiers=[
          "Programming Language :: Python",
          "Programming Language :: Python :: 3.3",
          "Development Status :: 4 - Beta",
          "Environment :: Console",
          "Intended Audience :: Information Technology",
          "License :: OSI Approved :: MIT License",
          "Operating System :: OS Independent",
          "Topic :: Text Processing :: Markup",
      ],
      )
